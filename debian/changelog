ocamldap (2.4.1-1) unstable; urgency=medium

  * Team upload
  * Update Homepage and debian/watch
  * New upstream release
  * Bump debhelper compat level to 13 and stop using cdbs
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Mon, 24 Aug 2020 17:13:25 +0200

ocamldap (2.1.8-11) unstable; urgency=medium

  * Team upload
  * Update Vcs-*
  * Fix compilation with OCaml 4.08.0

 -- Stéphane Glondu <glondu@debian.org>  Fri, 06 Sep 2019 09:30:35 +0200

ocamldap (2.1.8-10) unstable; urgency=medium

  * Fix FTBFS with OCaml 4.02.3

 -- Mehdi Dogguy <mehdi@debian.org>  Sat, 17 Oct 2015 20:50:38 +0200

ocamldap (2.1.8-9) unstable; urgency=low

  [ Stéphane Glondu ]
  * Team upload
  * Fix versioned build-deps to ensure smoother backports
  * Compile explicitly with pcre and compiler-libs (Closes: #731405)

  [ Sylvain Le Gall ]
  * Remove Sylvain Le Gall from uploaders

 -- Stéphane Glondu <glondu@debian.org>  Sat, 07 Dec 2013 13:20:13 +0100

ocamldap (2.1.8-8) unstable; urgency=low

  [ Mehdi Dogguy ]
  * Add ${ocaml:Provides} in debian/control

  [ Sylvain Le Gall ]
  * Put ocaml.mk at the beginning of debian/rules
  * Protect .cmi(s) by moving and restoring during build process

 -- Sylvain Le Gall <gildor@debian.org>  Sat, 19 Dec 2009 23:08:33 +0000

ocamldap (2.1.8-7) unstable; urgency=low

  * Add myself to uploaders
  * Change inclusion order in debian/rules to workaround a CDBS bug
  * Move the package to section ocaml
  * Remove old cmi files when cleaning (Closes: #549769)
  * Upgrade Standards-Version to 3.8.3
  * Use new features of dh-ocaml (>= 0.9)
    - Generate documentation using dh_ocamldoc

 -- Mehdi Dogguy <mehdi@debian.org>  Thu, 08 Oct 2009 23:18:01 +0200

ocamldap (2.1.8-6) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

  [ Sylvain Le Gall ]
  * Use ocaml 3.10.0-9 for generating .ocamldoc-apiref automatically
  * Switch packaging to git
  * Set maintainer to Debian OCaml Maintainers
  * Add dh-ocaml build-dependency (rules/ocaml.mk)
  * Upgrade debian/compat to 7 (use debian/clean)
  * Upgrade Standards-Version to 3.8.0 (debian/README.source)
  * Add Homepage field to debian/control
  * Use OCAML_OCAMLDOC_* variables to generate documentatio
  * Add ${misc:Depends} to dependencies
  * Update debian/copyright file to use
    http://wiki.debian.org/Proposals/CopyrightFormat
  * Remove obsolete debian/control.in related variables and files

 -- Sylvain Le Gall <gildor@debian.org>  Thu, 05 Mar 2009 01:12:21 +0100

ocamldap (2.1.8-5) unstable; urgency=low

  * Build for ocaml 3.10.0
  * Tighten build dependency for ocaml 3.10.0
  * Move standard documentation to standard OCaml documentation place

 -- Sylvain Le Gall <gildor@debian.org>  Tue, 18 Sep 2007 11:10:01 +0200

ocamldap (2.1.8-4) experimental; urgency=low

  * Add missing dependency on ocaml-nox

 -- Sylvain Le Gall <gildor@debian.org>  Tue, 24 Jul 2007 01:31:38 +0200

ocamldap (2.1.8-3) experimental; urgency=low

  * Upgrade debian/watch version to 3,
  * Upgrade debhelper debian/compat to 5,
  * Use CDBS for debian/rules,
  * Suppress dependency on libldap2-dev and chrpath, since package
    doesn't depend on it anymore,
  * Change email address to gildor@debian.org everywhere
  * Change watch URL to sf.net
  * Rebuild for ocaml 3.10.0

 -- Sylvain Le Gall <gildor@debian.org>  Tue, 24 Jul 2007 01:18:22 +0200

ocamldap (2.1.8-2) unstable; urgency=low

  * Removed control.in as per new ocaml policy.
  * Add XS-X-Vcs-Svn in control,
  * Rebuilt to fix dependency on pcre.cma (Closes: #387319)

 -- Sylvain Le Gall <gildor@debian.org>  Fri, 15 Sep 2006 00:10:29 +0200

ocamldap (2.1.8-1) unstable; urgency=low

  * New upstream release

 -- Sylvain Le Gall <gildor@debian.org>  Tue, 11 Jul 2006 23:18:43 +0200

ocamldap (2.1.7-2) unstable; urgency=low

  * Change my email address to gildor@debian.org,
  * Upgrade standards version to 3.7.2 (no change),
  * Made debian/control a PHONY target,

 -- Sylvain Le Gall <gildor@debian.org>  Tue, 13 Jun 2006 09:05:09 +0200

ocamldap (2.1.7-1) unstable; urgency=low

  * New upstream version

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Tue,  7 Feb 2006 00:51:08 +0100

ocamldap (2.1.6-2) unstable; urgency=low

  * Rebuild for OCaml 3.09.1

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Wed, 11 Jan 2006 00:03:44 +0100

ocamldap (2.1.6-1) unstable; urgency=low

  * New upstream release

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Tue, 13 Dec 2005 01:07:22 +0100

ocamldap (2.1.5-2) unstable; urgency=low

  * Minor cosmetic changes to the rule files
  * Remove hardcoded OCaml ABI

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Fri,  2 Dec 2005 22:46:26 +0100

ocamldap (2.1.5-1) unstable; urgency=low

  * New upstream release
  * Update information of copyright: new upstream website, license is LGPL
  * Set doc-base section to Apps/Programming
  * Update authors of doc-base
  * Remove libldap-ocaml.dirs, since it is now useless
  * Remove /usr/lib/ocaml/3.08.3/ from libldap-ocaml-dev.dirs (use ocamlfind
    insteed)

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Mon, 10 Oct 2005 21:46:15 +0200

ocamldap (2.1.4-1) unstable; urgency=low

  * New upstream release
  * Transition to svn-buildpackage

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Sun, 14 Aug 2005 02:23:27 +0200

ocamldap (2.1.3-1) unstable; urgency=low

  * New upstream release
  * Migration to Standards-Version 3.6.2.0 (no change)
  * Adapt the watch file so that it can be used by Debian QA
  * Add dependency on libocaml-ssl-dev
  * Remove the package libldap-ocaml: the library is now 100% pure OCaml
  * Use "ocamlfind ocamldoc -package ssl" to generate the documentation
  * Adpat debian/rules: removes the "mv" of the shared library (no more shared
    library)

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Fri, 22 Jul 2005 00:13:39 +0200

ocamldap (1.6.5-3) unstable; urgency=medium

  * Rebuild against ocamlnet 1.0

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Mon, 18 Apr 2005 20:44:23 +0200

ocamldap (1.6.5-2) unstable; urgency=medium

  * Transition to ocaml 3.08.3 : depends on ocaml-nox-3.08.3

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Wed, 30 Mar 2005 23:10:38 +0200

ocamldap (1.6.5-1) unstable; urgency=low

  * New upstream release

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Wed,  8 Dec 2004 01:14:35 +0100

ocamldap (1.6.1-2) unstable; urgency=low

  * Rebuilt against ocamlnet 0.98
  * debian/control
    - changed Deps and Build-Deps accordingly

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  9 Sep 2004 15:31:57 +0200

ocamldap (1.6.1-1) unstable; urgency=low

  * New upstream release
  * Transition to ocaml 3.08. Files modified :
    - control
    - libldap-ocaml-dev.dirs
    - rules

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Wed, 28 Jul 2004 00:21:47 +0200

ocamldap (1.4.6-1) unstable; urgency=low

  * New upstream release

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Mon, 10 May 2004 23:52:17 +0200

ocamldap (1.4.4-1) unstable; urgency=low

  * New upstream release

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Wed, 17 Mar 2004 00:22:13 +0100

ocamldap (1.4.3-1) unstable; urgency=low

  * New upstream release

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Mon, 23 Feb 2004 00:29:38 +0100

ocamldap (1.4.1-1) unstable; urgency=low

  * New upstream release
  * Correction of the FTBFS with patch make_bytecode ( Closes: #231166 ).

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Sun,  1 Feb 2004 21:00:15 +0100

ocamldap (1.3.2-1) unstable; urgency=low

  * New upstream version
  * makefile_findlib patch applied upstream don't use anymore
  * ocamldoc patch applied upstream don't user anymore
  * Move to ocaml-3.07 depends ( and install dir )

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Mon, 19 Jan 2004 21:41:25 +0100

ocamldap (1.1.1-2) unstable; urgency=low

  * Added a watch file

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Thu,  8 Jan 2004 17:25:09 +0100

ocamldap (1.1.1-1) unstable; urgency=low

  * New upstream release
  * Apply patches :
    * makefile_findlib to install in the good temp directory
  * Don't use debian/META anymore
  * Don't use debian/patches/makefile, since now we use findlib to 
  	install => add a dependency on ocaml-findlib

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Sun, 21 Sep 2003 17:08:59 +0200

ocamldap (1.1.0-1) unstable; urgency=low

  * First package
  * The name of the package in the archive will be libldap-ocaml
    following the ocaml policy convention
  * As it will be uploaded to unstable : closes: #203249
  * Add a META file 
  * Apply patches :
    * makefile to correct the makefile behavior when installing
    * ocamldoc to generate a better doc for the module

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Thu,  4 Sep 2003 00:05:07 +0200
